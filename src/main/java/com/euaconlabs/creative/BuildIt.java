package com.euaconlabs.creative;

import java.io.File;
import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;

import com.euaconlabs.creative.commands.DebugCommandExec;
import com.euaconlabs.creative.commands.PlotCommandExec;
import com.euaconlabs.creative.guard.BlockGuard;
import com.euaconlabs.creative.locale.Locale;
import com.euaconlabs.creative.plots.PlotManager;
import com.euaconlabs.creative.worldgen.BaseGenerator;

public class BuildIt extends JavaPlugin 
{
	/**
	 * The prefix to be displayed in front of every message.
	 */
	public static final String messagePrefix = ChatColor.GREEN + "[BuildIt] " + ChatColor.WHITE;
	
	/**
	 * The locale the plugin uses.
	 */
	private Locale locale = null;
	/**
	 * The plugin's configuration.
	 */
	private PluginConfig config = null;
	/**
	 * The plot manager.
	 */
	private PlotManager plotManager = null;
	
	@Override
	public void onEnable()
	{
		// Make sure our plugin directories exist:
		File f = getDataFolder();
		if(!f.exists())
			f.mkdirs();
		
		// Load the configuration:
		this.config = new PluginConfig();
		
		f = new File(f.toString() + File.separator + "config.cfg");
		if(!f.exists())
			this.config.create(f);
		else
			this.config.load(f);
		
		// Attempt to load the appropriate locale:
		f = new File(getDataFolder().toString() + File.separator + "locale");
		if(!f.exists())
			f.mkdir();
		
		this.locale = new Locale(this);
		if(!this.locale.load(this.config.getLocale()))
		{
			this.getServer().getPluginManager().disablePlugin(this);
			return;
		}
		
		// Initialize the plot manager:
		this.plotManager = new PlotManager(this);
		String worldFolder = getDataFolder().toString() + File.separator + "worlds";
		f = new File(worldFolder);
		if(!f.exists())
			f.mkdir();
		
		try
		{
			this.plotManager.initialize(worldFolder + File.separator + "plots");
			getLogger().info("Successfully initialized the PlotManager!");
		}
		catch(IOException e)
		{
			getLogger().severe("FATAL ERROR: Could not initialize plot manager!");
			getServer().getPluginManager().disablePlugin(this);
			return;
		}
		
		// Register interest in events:
		getServer().getPluginManager().registerEvents(new BlockGuard(this), this);
		
		// Register custom commands:
		getCommand("plot").setExecutor(new PlotCommandExec(this));
		getCommand("debug").setExecutor(new DebugCommandExec(this));
	}
	
	@Override
	public void onDisable()
	{
		try
		{
			this.plotManager.close();
		}
		catch(IOException e)
		{
			getLogger().severe("FATAL ERROR: Could not properly close plot manager! There could be data lost!");
		}
	}
	
	@Override
	public ChunkGenerator getDefaultWorldGenerator(String worldName, String id)
	{
		return new BaseGenerator(this.config.getPlotSizeX(), this.config.getPlotSizeZ(),
								 this.config.getRoadSizeX(), this.config.getRoadSizeZ(),
								 this.config.getRoadMaterial(), this.config.getBorderMaterial());
	}

	public Locale getLocale()
	{
		return locale;
	}

	public PluginConfig getConfiguration()
	{
		return config;
	}
	
	public PlotManager getPlotManager()
	{
		return plotManager;
	}
}
