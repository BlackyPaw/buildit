package com.euaconlabs.creative;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class PluginConfig
{
	private String locale = null;
	
	private int plotSizeX = 0;
	private int plotSizeZ = 0;
	
	private int roadSizeX = 0;
	private int roadSizeZ = 0;
	private byte roadMaterial = 0;
	
	private byte borderMaterial = 0;
	
	public PluginConfig()
	{
	}
	
	public String getLocale()
	{
		return locale;
	}

	public void setLocale(String locale)
	{
		this.locale = locale;
	}

	public int getPlotSizeX()
	{
		return plotSizeX;
	}

	public void setPlotSizeX(int plotSizeX)
	{
		this.plotSizeX = plotSizeX;
	}

	public int getPlotSizeZ()
	{
		return plotSizeZ;
	}

	public void setPlotSizeZ(int plotSizeZ)
	{
		this.plotSizeZ = plotSizeZ;
	}

	public int getRoadSizeX()
	{
		return roadSizeX;
	}

	public void setRoadSizeX(int roadSizeX)
	{
		this.roadSizeX = roadSizeX;
	}

	public int getRoadSizeZ()
	{
		return roadSizeZ;
	}

	public void setRoadSizeZ(int roadSizeZ)
	{
		this.roadSizeZ = roadSizeZ;
	}

	public byte getRoadMaterial()
	{
		return roadMaterial;
	}

	public void setRoadMaterial(byte roadMaterial)
	{
		this.roadMaterial = roadMaterial;
	}

	public byte getBorderMaterial()
	{
		return borderMaterial;
	}

	public void setBorderMaterial(byte borderMaterial)
	{
		this.borderMaterial = borderMaterial;
	}

	/**
	 * Initializes the configuration with default values and saves
	 * it to the given file.
	 * @param f
	 * @return
	 */
	public boolean create(File f)
	{
		this.locale = "en_US";
		
		this.plotSizeX = 35;
		this.plotSizeZ = 35;
		
		this.roadSizeX = 5;
		this.roadSizeZ = 5;
		this.roadMaterial = 5;
		
		this.borderMaterial = 44;
		
		return save(f);
	}
	
	/**
	 * Loads the configuration from the given file.
	 * @param file
	 */
	public boolean load(File f)
	{
		Properties props = new Properties();
		
		try
		{
			FileReader reader = new FileReader(f);
			props.load(reader);
			reader.close();
		}
		catch(IOException e)
		{
			return false;
		}
		
		this.locale = props.getProperty("locale");
		this.plotSizeX = Integer.valueOf(props.getProperty("plot.size.x"));
		this.plotSizeZ = Integer.valueOf(props.getProperty("plot.size.z"));
		this.roadSizeX = Integer.valueOf(props.getProperty("road.size.x"));
		this.roadSizeZ = Integer.valueOf(props.getProperty("road.size.z"));
		this.roadMaterial = Byte.valueOf(props.getProperty("road.material"));
		this.borderMaterial = Byte.valueOf(props.getProperty("border.material"));
		
		return true;
	}
	
	/**
	 * Saves the plugin's configuration.
	 * @param f
	 * @return
	 */
	public boolean save(File f)
	{
		Properties props = new Properties();
		
		props.put("locale", this.locale);
		props.put("plot.size.x", String.valueOf(this.plotSizeX));
		props.put("plot.size.z", String.valueOf(this.plotSizeZ));
		props.put("road.size.x", String.valueOf(this.roadSizeX));
		props.put("road.size.z", String.valueOf(this.roadSizeZ));
		props.put("road.material", String.valueOf(this.roadMaterial));
		props.put("border.material", String.valueOf(this.borderMaterial));
		
		try
		{
			FileWriter writer = new FileWriter(f);
			props.store(writer, "BuildIt Configuration");
			writer.close();
		}
		catch(IOException e)
		{
			return false;
		}
		
		return true;
	}
}
