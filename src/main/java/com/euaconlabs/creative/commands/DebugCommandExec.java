package com.euaconlabs.creative.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.euaconlabs.creative.BuildIt;

public class DebugCommandExec implements CommandExecutor
{
	private BuildIt plugin = null;
	
	public DebugCommandExec(BuildIt plugin)
	{
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if(args.length == 0)
			return false;
		
		String subcmd = args[0];
		boolean valid = true;
		
		if(subcmd.equalsIgnoreCase("disable"))
		{
			sender.sendMessage(BuildIt.messagePrefix + this.plugin.getLocale().getLocalizedString("buildit.debug.disable"));
			this.plugin.getServer().getPluginManager().disablePlugin(this.plugin);
		}
		else if(subcmd.equalsIgnoreCase("speed"))
		{
			if(args.length == 2)
			{
				if(sender instanceof Player)
				{
					Player p = (Player)sender;
					int s = 0;
					try
					{
						s = Integer.valueOf(args[1]);
						p.setWalkSpeed((float)(s) / 100.0f);
						p.setFlySpeed((float)(s) / 100.0f);
					}
					catch(NumberFormatException e)
					{
					}
				}
			}
		}
		
		return valid;
	}
}
