package com.euaconlabs.creative.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.euaconlabs.creative.BuildIt;
import com.euaconlabs.creative.plots.PlotDescription;
import com.euaconlabs.creative.util.Rectangle;

public class PlotCommandExec implements CommandExecutor
{

	private BuildIt plugin = null;
	
	public PlotCommandExec(BuildIt plugin)
	{
		this.plugin = plugin;
	}
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command  cmd, String label, String[] args)
	{
		if(args.length == 0 || !(sender instanceof Player))
			return false;
		
		String subcmd = args[0];
		Player player = (Player)sender;
		
		boolean valid = true;
		
		if(subcmd.equalsIgnoreCase("claim"))
		{
			PlotDescription plot = this.plugin.getPlotManager().claimPlot(player.getName());
			
			// Teleport the player onto his / her plot:
			Rectangle rect = plot.getRectangle();
			int x = rect.x1 + ((rect.x2 - rect.x1) >> 1);
			int z = rect.z1 + ((rect.z2 - rect.z1) >> 1);
			player.teleport(new Location(player.getWorld(), x, player.getWorld().getHighestBlockYAt(x, z), z));
			
			player.sendMessage(BuildIt.messagePrefix + this.plugin.getLocale().getLocalizedString("buildit.plot.claim"));
		}
		else if(subcmd.equalsIgnoreCase("home"))
		{
			int n = 0;
			
			if(args.length == 1)
				n = 1;
			else if(args.length == 2)
			{
				try
				{
					n = Integer.valueOf(args[1]);
				}
				catch(NumberFormatException e)
				{
					//...	
				}
			}
			else
			{
				player.sendMessage(BuildIt.messagePrefix + this.plugin.getLocale().getLocalizedString("buildit.cmd.invalidnargs"));
			}

			if(n <= 0)
				player.sendMessage(BuildIt.messagePrefix + this.plugin.getLocale().getLocalizedString("buildit.plot.home.notfound", String.valueOf(n)));
			else
			{
				PlotDescription plot = this.plugin.getPlotManager().getHomePlot(player.getName(), n);
				if(plot == null)
					player.sendMessage(BuildIt.messagePrefix + this.plugin.getLocale().getLocalizedString("buildit.plot.home.notfound", String.valueOf(n)));
				else
				{
					Rectangle rect = plot.getRectangle();
					int x = rect.x1 + ((rect.x2 - rect.x1) >> 1);
					int z = rect.z1 + ((rect.z2 - rect.z1) >> 1);
					player.teleport(new Location(player.getWorld(), x, player.getWorld().getHighestBlockYAt(x, z), z));
				}
			}
		}
		else if(subcmd.equalsIgnoreCase("add"))
		{
			if(args.length != 2)
				player.sendMessage(BuildIt.messagePrefix + this.plugin.getLocale().getLocalizedString("buildit.cmd.invalidnargs"));
			else
			{
				Location loc = player.getLocation();
				PlotDescription plot = this.plugin.getPlotManager().getPlotIfOwner(player.getName(), loc.getBlockX(), loc.getBlockZ());
				if(plot == null)
					player.sendMessage(BuildIt.messagePrefix + this.plugin.getLocale().getLocalizedString("buildit.plot.ownership"));
				else
				{
					if(!plot.addContributor(args[1]))
						player.sendMessage(BuildIt.messagePrefix + this.plugin.getLocale().getLocalizedString("buildit.plot.add.limit"));
					else
						player.sendMessage(BuildIt.messagePrefix + this.plugin.getLocale().getLocalizedString("buildit.plot.add", args[1]));
				}
			}
		}
		else if(subcmd.equalsIgnoreCase("remove"))
		{
			if(args.length != 2)
				player.sendMessage(BuildIt.messagePrefix + this.plugin.getLocale().getLocalizedString("buildit.cmd.invalidnargs"));
			else
			{
				Location loc = player.getLocation();
				PlotDescription plot = this.plugin.getPlotManager().getPlotIfOwner(player.getName(), loc.getBlockX(), loc.getBlockZ());
				if(plot == null)
					player.sendMessage(BuildIt.messagePrefix + this.plugin.getLocale().getLocalizedString("buildit.plot.ownership"));
				else
				{
					if(!plot.removeContributor(args[1]))
						player.sendMessage(BuildIt.messagePrefix + this.plugin.getLocale().getLocalizedString("buildit.plot.remove.notfound", args[1]));
					else
						player.sendMessage(BuildIt.messagePrefix + this.plugin.getLocale().getLocalizedString("buildit.plot.remove", args[1]));
				}
			}
		}
		else
		{
			player.sendMessage(BuildIt.messagePrefix + this.plugin.getLocale().getLocalizedString("buildit.plot.cmd.unknown", subcmd));
		}
		
		return valid;
	}

}
