package com.euaconlabs.creative.guard;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityPortalEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;

import com.euaconlabs.creative.BuildIt;

public class BlockGuard implements Listener
{
	private BuildIt plugin = null;
	
	public BlockGuard(BuildIt plugin)
	{
		this.plugin = plugin;
	}
	
	@EventHandler(ignoreCancelled = true)
	public void onBlockBreak(BlockBreakEvent e)
	{
		boolean canBuild = this.plugin.getPlotManager().canBuild(e.getPlayer().getName(), e.getBlock().getX(), e.getBlock().getZ());
		e.setCancelled(!canBuild);
	}
	
	@EventHandler(ignoreCancelled = true)
	public void onBlockPlace(BlockPlaceEvent e)
	{
		boolean canBuild = this.plugin.getPlotManager().canBuild(e.getPlayer().getName(), e.getBlock().getX(), e.getBlock().getZ());
		e.setCancelled(!canBuild);
	}
	
	@EventHandler(ignoreCancelled = true)
	public void onSignChange(SignChangeEvent e)
	{
		boolean canBuild = this.plugin.getPlotManager().canBuild(e.getPlayer().getName(), e.getBlock().getX(), e.getBlock().getZ());
		e.setCancelled(!canBuild);
	}
	
	@EventHandler(ignoreCancelled = true)
	public void onBlockDamage(BlockDamageEvent e)
	{
		boolean canBuild = this.plugin.getPlotManager().canBuild(e.getPlayer().getName(), e.getBlock().getX(), e.getBlock().getZ());
		e.setCancelled(!canBuild);
	}
	
	@EventHandler(ignoreCancelled = true)
	public void onBlockFade(BlockFadeEvent e)
	{
		// We do never want ice to melt:
		e.setCancelled(true);
	}
	
	@EventHandler(ignoreCancelled = true)
	public void onBlockBurn(BlockBurnEvent e)
	{
		// We never want any blocks to be destroyed due to fire:
		e.setCancelled(true);
	}
	
	@EventHandler(ignoreCancelled = true)
	public void onRedstone(BlockRedstoneEvent e)
	{
		// Disable any redstone functionalities:
		e.setNewCurrent(e.getOldCurrent());
	}
	
	@EventHandler(ignoreCancelled = true)
	public void onExplosion(ExplosionPrimeEvent e)
	{
		e.setCancelled(true);
	}
	
	@EventHandler(ignoreCancelled = true)
	public void onProjectile(ProjectileLaunchEvent e)
	{
		e.setCancelled(true);
	}
	
	@EventHandler(ignoreCancelled = true)
	public void onTeleport(EntityPortalEvent e)
	{
		e.setCancelled(true);
	}
}
