package com.euaconlabs.creative.locale;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import com.euaconlabs.creative.BuildIt;

public class Locale
{
	/**
	 * A reference to the plugin itself.
	 */
	private BuildIt plugin = null;
	/**
	 * The map containing all localized strings.
	 */
	private HashMap<String, String> localizedStrings = new HashMap<String, String>();
	
	public Locale(BuildIt plugin)
	{
		this.plugin = plugin;
	}
	
	/**
	 * Loads the localized string with the given name.
	 * @param key
	 * @return
	 */
	public String getLocalizedString(String key, String ... inputs)
	{
		String s = localizedStrings.get(key);
		for(int i = 0; i < inputs.length; ++i)
		{
			s = s.replaceAll("%" + String.valueOf(i), inputs[i]);
		}
		return (s == null ? "<unknown>" : s);
	}
	
	/**
	 * Loads the locale file corresponding to the given locale. If the locale
	 * could not be found, it falls back to the default built-in locale.
	 * @param locale
	 * @return
	 */
	public boolean load(String locale)
	{
		this.localizedStrings.clear();
		
		File f = new File(this.plugin.getDataFolder() + File.separator + "locale" + File.separator + "locale_" + locale + ".lang");
		if(!f.exists())
		{
			this.plugin.getLogger().warning("Locale " + locale + " was not found. Using default locale instead!");
			InputStream in = Locale.class.getResourceAsStream("/locale/default.lang");
			if(in == null)
			{
				this.plugin.getLogger().severe("FATAL ERROR: The default locale is missing!");
				return false;
			}
			return this.doLoad(in);
		}
		else
		{
			FileInputStream in;
			try
			{
				in = new FileInputStream(f);
			} catch (FileNotFoundException e)
			{
				this.plugin.getLogger().severe("FATAL ERROR: The locale " + locale + " could not be opened!");
				return false;
			}
			return this.doLoad(in);
		}
	}

	private boolean doLoad(InputStream in)
	{
		Properties props = new Properties();
		try
		{
			props.load(in);
		}
		catch(IOException e)
		{
			return false;
		}
		finally
		{
			try
			{
				in.close();
			} catch (IOException e)
			{
				//...
			}
		}
		
		Iterator<Object> it = props.keySet().iterator();
		while(it.hasNext())
		{
			String key = (String)it.next();
			this.localizedStrings.put(key, props.getProperty(key));
		}
		
		return true;
	}
}
