package com.euaconlabs.creative.plots;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.euaconlabs.creative.util.Rectangle;

/**
 * A standalone database containing information about any player playing
 * on a given world.
 * @author BlackyPaw
 *
 */
public class PlotDB
{
	/**
	 * The connection to the SQLite database.
	 */
	private Connection connection = null;
	
	public PlotDB()
	{
	}
	
	/**
	 * Gets the number of plots the specified user owns.
	 * @param user
	 * @return
	 */
	public int getPlayerNumPlots(String user)
	{
		try
		{
			Statement statement = this.connection.createStatement();
			String query = "SELECT id FROM PlotDB WHERE owner='"+user+"';";
			ResultSet rs = statement.executeQuery(query);
			
			if(rs.next())
				return rs.getInt(1);
			
			return 0;
		}
		catch (SQLException e)
		{
			return 0;
		}
	}
	
	/**
	 * Returns a list of all plots found in the PlotDB.
	 * @return
	 */
	public ArrayList<PlotDescription> getPlots()
	{
		try
		{
			Statement statement = this.connection.createStatement();
			String query = "SELECT * FROM PlotDB;";
			ResultSet rs = statement.executeQuery(query);
			
			ArrayList<PlotDescription> plots = new ArrayList<PlotDescription>();
			while(rs.next())
			{
				Rectangle rect = new Rectangle();
				rect.x1 = rs.getInt("x1");
				rect.x2 = rs.getInt("x2");
				rect.z1 = rs.getInt("z1");
				rect.z2 = rs.getInt("z2");
				PlotDescription plot = new PlotDescription(rs.getInt("id"), rs.getString("owner"), rect);
				
				String contributors = rs.getString("contributors");
				if(!contributors.equals(' '))
				{
					String[] players = contributors.split(";");
					for(int i = 0; i < players.length; ++i)
					{
						plot.addContributor(players[i]);
					}
				}
				
				plot.setHomeID(rs.getInt("homeid"));
				
				plots.add(plot);
			}
			
			return plots;
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Writes all plots found in the given list to the database. If they
	 * already exist their values will be updated, otherwise a new row
	 * will be inserted for them.
	 * @param plots
	 */
	public boolean writePlots(List<PlotDescription> plots)
	{
		try
		{
			this.connection.setAutoCommit(false);
			Iterator<PlotDescription> it = plots.iterator();
			
			while(it.hasNext())
			{
				PlotDescription plot = it.next();

				StringBuilder builder = new StringBuilder(67);
				int counter = 0;
				Iterator<String> it2 = plot.getContributors().iterator();
				while(it2.hasNext())
				{
					builder.append(it2.next());
					if(counter != plot.getContributors().size() - 1)
						builder.append(";");
					counter++;
				}
				
				Rectangle rect = plot.getRectangle();
				
				Statement statement = this.connection.createStatement();
				String update = "INSERT OR REPLACE INTO PlotDB (id, owner, contributors, x1, x2, z1, z2, homeid) VALUES "
						+ "(" + plot.getID() + ", '" + plot.getOwner() + "', '" + builder.toString() + "', "
						+ rect.x1 + ", " + rect.x2 + ", " + rect.z1 + ", " + rect.z2 + ", " + plot.getHomeID() + ");";
				statement.executeUpdate(update);
			}
			
			this.connection.setAutoCommit(true);
			return true;
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Closes the connection to the database.
	 */
	public void close()
	{
		try
		{
			this.connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Loads the database from the given file.
	 * @param f
	 * @return
	 */
	public boolean load(File f)
	{
		try
		{
			Class.forName("org.sqlite.JDBC");
			this.connection = DriverManager.getConnection("jdbc:sqlite:" + f.toString());
			
			String ensureTable = "create table if not exists PlotDB (id INTEGER, owner VARCHAR(16), contributors VARCHAR(67), x1 INTEGER, x2 INTEGER, z1 INTEGER, z2 INTEGER, homeid INTEGER, PRIMARY KEY (id))";
			
			Statement statement = this.connection.createStatement();
			statement.executeUpdate(ensureTable);
			statement.close();
			
			return true;
		}
		catch(Exception e)
		{
			if(this.connection != null)
			{
				try
				{
					this.connection.close();
				}
				catch(Exception e2)
				{
					//...
				}
			}
			return false;
		}
	}
}
