package com.euaconlabs.creative.plots;

import java.util.ArrayList;
import java.util.List;

import com.euaconlabs.creative.util.Rectangle;

public class PlotDescription
{
	private int id = 0;
	private String owner = null;
	private int homeID = 0;
	private List<String> contributors = new ArrayList<String>(4);
	private Rectangle rect = null;
	
	public PlotDescription(int id, String owner, Rectangle rect)
	{
		this.id = id;
		this.owner = owner;
		this.rect = rect;
	}
	
	/**
	 * Adds a contributor to the plot.
	 * @param contrib
	 * @return True if the contributor could be added; False if the limit of
	 * 		   contributors has been reached.
	 */
	public boolean addContributor(String contrib)
	{
		if(contributors.size() < 4)
		{
			contributors.add(contrib);
			return true;
		}
		return false;
	}
	
	/**
	 * Removes a contributor from the plot.
	 * @param contrib
	 * @return True if the contributor was found and removed; False if the
	 * 		   contributor was not found.
	 */
	public boolean removeContributor(String contrib)
	{
		if(contributors.contains(contrib))
		{
			contributors.remove(contrib);
			return true;
		}
		return false;
	}
	
	public int getID()
	{
		return this.id;
	}
	
	public String getOwner()
	{
		return this.owner;
	}
	
	public int getHomeID()
	{
		return this.homeID;
	}
	
	public void setHomeID(int homeID)
	{
		this.homeID = homeID;
	}
	
	public List<String> getContributors()
	{
		return this.contributors;
	}
	
	public Rectangle getRectangle()
	{
		return this.rect;
	}
	
	/**
	 * Checks whether the specified player is allowed to build on this plot
	 * or not.
	 * @param user
	 * @return
	 */
	public boolean canBuild(String user)
	{
		return (this.owner.equals(user) || this.contributors.contains(user));
	}
}
