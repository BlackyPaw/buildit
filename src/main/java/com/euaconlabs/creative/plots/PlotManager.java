package com.euaconlabs.creative.plots;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.euaconlabs.creative.BuildIt;
import com.euaconlabs.creative.util.Numerics;
import com.euaconlabs.creative.util.Rectangle;

public class PlotManager
{
	private BuildIt plugin = null;
	
	/**
	 * The name of the plot manager files without their respective extensions.
	 */
	private String fileWithoutExt = null;
	/**
	 * The list of all plots.
	 */
	private List<PlotDescription> allPlots = Collections.synchronizedList(new ArrayList<PlotDescription>());
	/**
	 * A specialized map which holds the list of all plots a player may build
	 * on.
	 */
	private Map<String, ArrayList<PlotDescription>> playerPlots = Collections.synchronizedMap(new HashMap<String, ArrayList<PlotDescription>>());
	/**
	 * The plotDB of the plot manager.
	 */
	private PlotDB plotDB = new PlotDB();
	/**
	 * The ID of the next plot available.
	 * A plot ID is encoded in the follwing format:
	 * Bits:
	 * 0  -  1		State flag
	 * 2  - 16		Z Coordinate; bit  2 = sign,  3 - 16 = value
	 * 17 - 31		X Coordinate; bit 17 = sign, 18 - 31 = value
	 */
	private int nextPlotID = 0;
	
	public PlotManager(BuildIt plugin)
	{
		this.plugin = plugin;
	}
	
	/**
	 * Opens the PlotDB and loads the plot manager's state from a file.
	 * @param fileWithoutExt The file name of the database file to load WITHOUT
	 * 						 the file extension (!).
	 * @throws IOException
	 */
	public void initialize(String fileWithoutExt) throws IOException
	{
		this.fileWithoutExt = fileWithoutExt;
		if(!this.plotDB.load(new File(fileWithoutExt + ".db")))
			throw new IOException("Failed to load / create PlotDB!");
		
		this.plugin.getLogger().info("Loading plots from PlotDB...");
		this.allPlots = this.plotDB.getPlots();
		if(this.allPlots == null)
			throw new IOException("Failed to load plots from PlotDB!");
		
		this.plugin.getLogger().info("Loaded " + this.allPlots.size() + " plots!");
		
		// Now sort by player, too:
		Iterator<PlotDescription> it = this.allPlots.iterator();
		while(it.hasNext())
		{
			PlotDescription plot = it.next();
			
			String owner = plot.getOwner();
			if(!this.playerPlots.containsKey(owner))
				this.playerPlots.put(owner, new ArrayList<PlotDescription>());
			
			this.playerPlots.get(owner).add(plot);
			
			List<String> contributors = plot.getContributors();
			Iterator<String> it2 = contributors.iterator();
			
			while(it2.hasNext())
			{
				String contrib = it2.next();
				if(!this.playerPlots.containsKey(contrib))
					this.playerPlots.put(contrib, new ArrayList<PlotDescription>());
				
				this.playerPlots.get(contrib).add(plot);
			}
		}
		
		// Load the plotmanager state:
		File cfg = new File(fileWithoutExt + ".inf");
		if(cfg.exists())
		{
			Properties props = new Properties();
				FileReader reader = new FileReader(cfg);
				props.load(reader);
				reader.close();
				
			this.nextPlotID = Integer.valueOf(props.getProperty("plotID"));
		}
		else
		{
			this.nextPlotID = 0;
		}
	}
	
	/**
	 * Closes the plot manager and stores everything into the PlotDB.
	 */
	public void close() throws IOException
	{
		this.plugin.getLogger().info("Writing " + this.allPlots.size() + " plots to PlotDB...");
		this.plotDB.writePlots(this.allPlots);
		this.plugin.getLogger().info("Closing PlotDB...");
		this.plotDB.close();
		
		this.plugin.getLogger().info("Saving current plot manager state...");
		Properties props = new Properties();
		props.setProperty("plotID", String.valueOf(this.nextPlotID));
		
		FileWriter writer = new FileWriter(new File(this.fileWithoutExt + ".inf"));
		props.store(writer, "DO NOT CHANGE ANY OF THESE VALUES!!!!!");
	}
	
	/**
	 * Claims a plot for the given user.
	 * @param user
	 * @return
	 */
	public PlotDescription claimPlot(String user)
	{
		int plotID = nextPlotID();
		Rectangle rect = getPlotRect(plotID);
		int homeID = this.getNumHomePlots(user);
		PlotDescription plot = new PlotDescription(plotID, user, rect);
		plot.setHomeID(homeID);
		
		// Insert the plot into the plot manager:
		this.allPlots.add(plot);
		
		if(!this.playerPlots.containsKey(user))
			this.playerPlots.put(user, new ArrayList<PlotDescription>());
		
		this.playerPlots.get(user).add(plot);
		
		return plot;
	}
	
	/**
	 * Returns the number of plots a player owns.
	 * @param user
	 * @return
	 */
	public int getNumHomePlots(String user)
	{
		ArrayList<PlotDescription> list = this.playerPlots.get(user);
		if(list == null)
			return 0;
		
		int count = 0;
		for(int i = 0; i < list.size(); ++i)
		{
			if(list.get(i).getOwner().equals(user))
				count++;
		}
		return count;
	}
	
	/**
	 * Returns the n-th home plot of the specified player.
	 * @param user
	 * @param n
	 * @return
	 */
	public PlotDescription getHomePlot(String user, int n)
	{
		ArrayList<PlotDescription> list = this.playerPlots.get(user);
		if(list == null)
			return null;
		
		int count = 0;
		int i;
		for(i = 0; i < list.size() && count < n; ++i)
		{
			if(list.get(i).getOwner().equals(user))
				count++;
		}
		if(count > 0)
			return list.get(i-1);
		return null;
	}
	
	/**
	 * Gets the plot lying under the given coordinates if the specified user
	 * is its owner.
	 * @param user
	 * @param x
	 * @param z
	 * @return Returns the plot if found; Null if the user is not the plot's owner
	 * 		   or the plot does not exist.
	 */
	public PlotDescription getPlotIfOwner(String user, int x, int z)
	{
		ArrayList<PlotDescription> list = this.playerPlots.get(user);
		if(list == null)
			return null;
		
		for(int i = 0; i < list.size(); ++i)
		{
			PlotDescription plot = list.get(i);
			if(plot.getRectangle().inside(x, z))
				return plot;
		}
		return null;
	}
	
	/**
	 * Returns the ID of the next available plot an generates a new one.
	 * @return
	 */
	private int nextPlotID()
	{
		int result = this.nextPlotID;
		
		byte state = (byte)((this.nextPlotID >> 30) & 0x03);
		
		// Decode the values for X and Z:
		short x = (short)((this.nextPlotID & 0x1FFF8000) >> 15);
		if((this.nextPlotID & 0x20000000) != 0x00)
			x = (short) -x;
		
		short z = (short)(this.nextPlotID & 0x00003FFF);
		if((this.nextPlotID & 0x00004000) != 0x00)
			z = (short) -z;
		
		if(state == 0x00)
		{
			x++;
			state = (byte) (x < (Numerics.abs(z) + 1) ? 0x00 : 0x01);
		}
		else if(state == 0x01)
		{
			z++;
			state = (byte) (z < x ? 0x01 : 0x02);
		}
		else if(state == 0x02)
		{
			x--;
			state = (byte) (x > -z ? 0x02 : 0x03);
		}
		else if(state == 0x03)
		{
			z--;
			state = (byte) (z > x ? 0x03 : 0x00);
		}
		
		// Compose the bits and bytes again to the next ID:
		this.nextPlotID = 0;
		this.nextPlotID |= ((int)(state) & 0x03) << 30;
		if(x < 0)
			this.nextPlotID |= (((int)(-x) & 0x3FFF) << 15) | 0x20000000;
		else
			this.nextPlotID |= (((int)(x) & 0x3FFF) << 15); 
		
		if(z < 0)
			this.nextPlotID |= (((int)(-z) & 0x3FFF)) | 0x00004000;
		else
			this.nextPlotID |= (((int)(z) & 0x3FFF));
		
		return result;
	}
	
	/**
	 * Calculates the rectangle of where the block with the given ID resides.
	 * @param plotID
	 * @return
	 */
	private Rectangle getPlotRect(int plotID)
	{
		// Decode the values for X and Z:
		short x = (short)((plotID & 0x1FFF8000) >> 15);
		if((plotID & 0x20000000) != 0x00)
			x = (short) -x;
				
		short z = (short)(plotID & 0x00003FFF);
		if((plotID & 0x00004000) != 0x00)
			z = (short) -z;
		
		int plotX = this.plugin.getConfiguration().getPlotSizeX();
		int plotZ = this.plugin.getConfiguration().getPlotSizeZ();
		
		int roadX = this.plugin.getConfiguration().getRoadSizeX();
		int roadZ = this.plugin.getConfiguration().getRoadSizeZ();
		
		int offX = (x >= 0 ? roadX : roadX);
		int offZ = (z >= 0 ? roadZ : roadZ);
		
		int factorX = plotX + roadX + 2;
		int factorZ = plotZ + roadZ + 2;
		
		Rectangle result = new Rectangle();
		result.x1 = factorX * x + offX + 1;
		result.x2 = result.x1 + plotX - 1;
		result.z1 = factorZ * z + offZ + 1;
		result.z2 = result.z1 + plotZ - 1;
		
		return result;
	}
	
	/**
	 * Checks whether the specified user can build at the given position.
	 * @param user
	 * @param x
	 * @param z
	 * @return
	 */
	public boolean canBuild(String user, int x, int z)
	{
		ArrayList<PlotDescription> plots = this.playerPlots.get(user);
		if(plots == null)
			return false;
		
		for(int i = 0; i < plots.size(); ++i)
		{
			PlotDescription plot = plots.get(i);
			if(plot.getRectangle().inside(x, z))
				return true;
		}
		return false;
	}
}
