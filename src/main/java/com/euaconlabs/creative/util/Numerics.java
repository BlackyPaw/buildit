package com.euaconlabs.creative.util;

public class Numerics
{
	public static short abs(short s)
	{
		return (short) (s < 0 ? -s : s);
	}
}
