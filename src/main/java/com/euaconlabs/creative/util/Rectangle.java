package com.euaconlabs.creative.util;

/**
 * Simple rectangle utility.
 * @author BlackyPaw
 *
 */
public class Rectangle
{
	public int x1, x2;
	public int z1, z2;
	
	@Override
	public boolean equals(Object obj)
	{
		if(!(obj instanceof Rectangle))
			return false;
		
		Rectangle r = (Rectangle)obj;
		return (x1 == r.x1 && x2 == r.x2 && z1 == r.z1 && z2 == r.z2);
	}
	
	public boolean inside(int x, int z)
	{
		return (x >= x1 && x <= x2 && z >= z1 && z <= z2);
	}
}
