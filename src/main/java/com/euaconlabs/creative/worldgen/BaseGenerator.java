package com.euaconlabs.creative.worldgen;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;

import com.euaconlabs.creative.worldgen.PlotPopulator;

public class BaseGenerator extends ChunkGenerator
{
	private int plotSizeX = 0;
	private int plotSizeZ = 0;
	private int roadSizeX = 0;
	private int roadSizeZ = 0;
	private byte roadMaterial = 0;
	private byte borderMaterial = 0;
	
	private PlotPopulator populator = new PlotPopulator();
	
	public BaseGenerator()
	{
	}
	
	public BaseGenerator(int plotSizeX, int plotSizeZ, int roadSizeX, int roadSizeZ, byte roadMaterial, byte borderMaterial)
	{
		this.plotSizeX = plotSizeX;
		this.plotSizeZ = plotSizeZ;
		this.roadSizeX = roadSizeX;
		this.roadSizeZ = roadSizeZ;
		this.roadMaterial = roadMaterial;
		this.borderMaterial = borderMaterial;
		this.populator.setRoadMaterial(this.roadMaterial);
		this.populator.setBorderMaterial(this.borderMaterial);
	}
	
	public int getPlotSizeX()
	{
		return plotSizeX;
	}

	public void setPlotSizeX(int plotSizeX)
	{
		this.plotSizeX = plotSizeX;
	}

	public int getPlotSizeZ()
	{
		return plotSizeZ;
	}

	public void setPlotSizeZ(int plotSizeZ)
	{
		this.plotSizeZ = plotSizeZ;
	}

	public int getRoadSizeX()
	{
		return roadSizeX;
	}

	public void setRoadSizeX(int roadSizeX)
	{
		this.roadSizeX = roadSizeX;
	}

	public int getRoadSizeZ()
	{
		return roadSizeZ;
	}

	public void setRoadSizeZ(int roadSizeZ)
	{
		this.roadSizeZ = roadSizeZ;
	}

	public byte getRoadMaterial()
	{
		return roadMaterial;
	}

	public void setRoadMaterial(byte roadMaterial)
	{
		this.roadMaterial = roadMaterial;
		this.populator.setRoadMaterial(this.roadMaterial);
	}

	public byte getBorderMaterial()
	{
		return borderMaterial;
	}

	public void setBorderMaterial(byte borderMaterial)
	{
		this.borderMaterial = borderMaterial;
		this.populator.setBorderMaterial(this.borderMaterial);
	}

	public static void setBlock(byte[][] chunk, int x, int y, int z, byte id)
	{
		if(chunk[y >> 4] == null)
		{
			chunk[y >> 4] = new byte[4096];
		}
		
		chunk[y >> 4][((y & 0xF) << 8) | (z << 4) | x] = id;
	}
	
	public static byte getBlock(byte[][] chunk, int x, int y, int z)
	{
		if(chunk[y >> 4] == null)
		{
			return (byte) 0;
		}
		
		return chunk[y >> 4][((y & 0xF) << 8) | (z << 4) | x];
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public byte[][] generateBlockSections(World world, Random random, int cx, int cz, BiomeGrid biome)
	{
		byte[][] result = new byte[world.getMaxHeight() / 16][];
		
		// Base generation:
		for(int x = 0; x < 16; ++x)
		{
			for(int z = 0; z < 16; ++z)
			{
				// Generate the bedrock:
				setBlock(result, x, 0, z, (byte)Material.BEDROCK.getId());
				// Generate the dirt:
				for(int y = 1; y < 64; ++y)
					setBlock(result, x, y, z, (byte)Material.DIRT.getId());
				// Generate the grass on top:
				setBlock(result, x, 64, z, (byte)Material.GRASS.getId());
			}
		}
		
		int fx = plotSizeX + roadSizeX + 2;
		int fz = plotSizeZ + roadSizeZ + 2;
		
		// Road generation:
		int nextRoadX = (int) Math.round(((float)cx * 16.0f) / (float)(fx));
		int blockX = nextRoadX * fx - cx * 16;
		
		int nextRoadZ = (int) Math.round(((float)cz * 16.0f) / (float)(fz));
		int blockZ = nextRoadZ * fz - cz * 16;
		
		int startX  = blockX;
		int lengthX = roadSizeX;
		
		int startZ  = blockZ;
		int lengthZ = roadSizeZ;
		
		if(startX < 0 && startX + roadSizeX >= 0)
		{
			lengthX = startX + roadSizeX;
			startX  = 0;
		}
		
		if(startZ < 0 && startZ + roadSizeZ >= 0)
		{
			lengthZ = startZ + roadSizeZ;
			startZ  = 0;
		}
		
		if(startX >= 0)
		{
			for(int i = startX; i < startX + lengthX && i < 16; ++i)
			{
				for(int k = 0; k < 16; ++k)
				{
					setBlock(result, i, 64, k, roadMaterial);
				}
			}
		}
		
		if(startZ >= 0)
		{
			for(int i = 0; i < 16; ++i)
			{
				for(int k = startZ; k < startZ + lengthZ && k < 16; ++k)
				{
					setBlock(result, i, 64, k, roadMaterial);
				}
			}
		}
		
		return result;
	}
	
	@Override
	public List<BlockPopulator> getDefaultPopulators(World world)
	{
		return Arrays.asList((BlockPopulator)this.populator);
	}
	
	@Override
	public Location getFixedSpawnLocation(World world, Random random)
	{
		return new Location(world, 0, world.getHighestBlockYAt(0, 0), 0);
	}
}