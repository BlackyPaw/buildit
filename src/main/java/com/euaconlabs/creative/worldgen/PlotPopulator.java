package com.euaconlabs.creative.worldgen;

import java.util.Random;

import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;

public class PlotPopulator extends BlockPopulator
{
	private byte roadMaterial = 0;
	private byte borderMaterial = 0;
	
	@SuppressWarnings("deprecation")
	public static Material getMaterialFromID(byte id)
	{
		Material[] values = Material.values();
		for(Material m : values)
		{
			if(m.getId() == id)
				return m;
		}
		return null;
	}
	
	public PlotPopulator()
	{
	}

	public byte getRoadMaterial()
	{
		return roadMaterial;
	}

	public void setRoadMaterial(byte roadMaterial)
	{
		this.roadMaterial = roadMaterial;
	}

	public byte getBorderMaterial()
	{
		return borderMaterial;
	}

	public void setBorderMaterial(byte borderMaterial)
	{
		this.borderMaterial = borderMaterial;
	}

	@Override
	public void populate(World world, Random random, Chunk source)
	{
		int cx = source.getX() * 16;
		int cz = source.getZ() * 16;
		
		Material road = getMaterialFromID(this.roadMaterial);
		Material border = getMaterialFromID(this.borderMaterial);
		
		// Generate the border:
		for(int i = cx; i < cx + 16; ++i)
		{
			for(int j = cz; j < cz + 16; ++j)
			{
				if(world.getBlockAt(i, 64, j).getType() == road)
				{
					// Check for any neighbours:
					if(world.getBlockAt(i - 1, 64, j).getType() == Material.GRASS)
						world.getBlockAt(i - 1, 65, j).setType(border);
					if(world.getBlockAt(i + 1, 64, j).getType() == Material.GRASS)
						world.getBlockAt(i + 1, 65, j).setType(border);
					if(world.getBlockAt(i, 64, j - 1).getType() == Material.GRASS)
						world.getBlockAt(i, 65, j - 1).setType(border);
					if(world.getBlockAt(i, 64, j + 1).getType() == Material.GRASS)
						world.getBlockAt(i, 65, j + 1).setType(border);
				}
			}
		}
	}
	
}
